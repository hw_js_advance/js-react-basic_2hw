import React from "react";
import { Modal } from "./Components/Modal/Modal.js";
import { Cards } from "./Components/Cards/Cards.js";
import { Header } from "./Components/Header/Header.js";
import "./Components/global.scss";
import "./Components/reset.scss";
import "./Components/Cards/cards.scss";
import "./Components/Card/card.scss";
import "./Components/Header/header.scss";
import "./Components/Modal/modal.scss";
import "./Components/Order/order.scss";
import "./Components/Button/button.scss";

class App extends React.Component {
  state = {
    cards: [],
    orders: [],
    favorites: [],
    isLoading: false,
    openCartModal: false,
    productToCart: null,
    countFav: 0,
    countCart: 0,
  };

  componentDidMount() {
    this.setState({ isLoading: true });

    const saveOrders = JSON.parse(localStorage.getItem("orders"));
    if (saveOrders !== null) {
      this.setState({ orders: saveOrders });
    }

    const saveFav = JSON.parse(localStorage.getItem("favorites"));
    if (saveFav !== null) {
      this.setState({ favorites: saveFav });
    }

    const saveCountFav = JSON.parse(localStorage.getItem("counter fav"));
    if (saveCountFav !== null) {
      this.setState({ countFav: saveCountFav });
    }

    const saveCountCart = JSON.parse(localStorage.getItem("counter cart"));
    if (saveCountCart !== null) {
      this.setState({ countCart: saveCountCart });
    }

    fetch("zoo.json")
      .then((r) => r.json())
      .then((data) => {
        this.setState({ isLoading: false, cards: data });
        console.log("data", data);
      });
  }

  componentDidUpdate() {
    localStorage.setItem("orders", JSON.stringify(this.state.orders));
    localStorage.setItem("favorites", JSON.stringify(this.state.favorites));
    localStorage.setItem("counter fav", JSON.stringify(this.state.countFav));
    localStorage.setItem("counter cart", JSON.stringify(this.state.countCart));
  }

  openCartModal = (product) => {
    this.setState({
      openCartModal: true,
      productToCart: product,
    });
  };

  addToOrder = () => {
    this.setState({
      orders: [...this.state.orders, this.state.productToCart],
      productToCart: null,
      countCart: this.state.countCart + 1,
    });
  };

  addToFavorites = (product) => {
    this.setState({
      favorites: [...this.state.favorites, product],
      countFav: this.state.countFav + 1,
    });
  };

  render() {
    return (
      <div className="app container">
        <Header
          orders={this.state.orders}
          favorites={this.state.favorites}
          countFav={this.state.countFav}
          countCart={this.state.countCart}
        />
        {this.state.isLoading && <p>Loading...</p>}
        <Cards
          cards={this.state.cards}
          onAdd={this.addToOrder}
          onAddFav={this.addToFavorites}
          openCartModal={this.openCartModal}
        />
        {this.state.openCartModal && (
          <Modal
            className="modal"
            closeButton={() => this.setState({ openCartModal: false })}
            handleClick={this.addToOrder}
            product={this.state.productToCart}
            header=""
            text="Покласти в кошик ?"
          />
        )}
      </div>
    );
  }
}
export default App;
