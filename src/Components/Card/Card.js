import React from "react";
import PropTypes from "prop-types";
import { Button } from "../Button/Button.js";
import { FaRegHeart } from "react-icons/fa";

export class Card extends React.Component {
  state = { favOpen: false };

  render() {
    return (
      <li className="card">
        <img src={"./img/" + this.props.card.img} alt="" />
        <span>Артикул:{this.props.card.article}</span>
        <p>{this.props.card.name}</p>
        <b>{this.props.card.price} грн.</b>
        <Button
          className="btn"
          text="Add to cart"
          backgroundColor="rgb(177,210,5)"
          handleClick={() => this.props.openCartModal(this.props.card)}
        />
        <span class="fav-logo">
          <FaRegHeart
            className={"favorite-btn"}
            onClick={() => this.props.onAddFav(this.props.card)}
          />
          В бажання
        </span>
      </li>
    );
  }
}

Card.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
    article: PropTypes.number.isRequired,
    img: PropTypes.string.isRequired,
  }).isRequired,
};

Card.defaultProps = {
  card: PropTypes.shape({
    name: "No Name",
    img: "No image",
  }),
};
