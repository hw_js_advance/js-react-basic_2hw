import React from "react";
import { Card } from "../Card/Card.js";

export class Cards extends React.Component {
  render() {
    return (
      <ul className="cards">
        {this.props.cards.map((card) => (
          <Card
            key={card.article}
            card={card}
            openCartModal={this.props.openCartModal}
            onAddFav={this.props.onAddFav}
          />
        ))}
      </ul>
    );
  }
}
