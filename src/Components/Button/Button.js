import React from "react";

export class Button extends React.Component {
  render() {
    return (
      <div className="btn-block">
        <div
          className={this.props.className}
          style={{ backgroundColor: this.props.backgroundColor }}
          onClick={() => this.props.handleClick()}
        >
          {this.props.text}
        </div>
      </div>
    );
  }
}
