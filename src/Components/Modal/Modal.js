import React from "react";
import { Button } from "../Button/Button.js";

export class Modal extends React.Component {
  render() {
    return (
      <div className="modal-wrapper" onClick={() => this.props.closeButton()}>
        <div
          className={`modal ${this.props.className}`}
          onClick={(e) => e.stopPropagation()}
        >
          <h2>{this.props.header}</h2>
          <p>{this.props.text}</p>
          <div className="btn-close" onClick={() => this.props.closeButton()}>
            {this.props.closeButton}
          </div>
          <Button
            className="modal-btn"
            text="Покласти в Кошик"
            handleClick={this.props.handleClick}
          />
        </div>
      </div>
    );
  }
}
