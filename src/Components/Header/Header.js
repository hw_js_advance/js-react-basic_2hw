import React from "react";
import { FaShoppingCart } from "react-icons/fa";
import { FaRegHeart } from "react-icons/fa";
import { Order } from "../Order/Order.js";
import { Favorite } from "../Favorite/Favorite.js";

export class Header extends React.Component {
  state = { cartOpen: false, favOpen: false, removeProductCart: false };

  favOpenClick = () => {
    this.setState((prevstate) => ({
      favOpen: !prevstate.favOpen,
    }));
  };

  cartOpenClick = () => {
    this.setState((prevstate) => ({
      cartOpen: !prevstate.cartOpen,
    }));
  };

  render() {
    return (
      <header>
        <div className="header-block container">
          <div className="logo">Zoo-market</div>
          <ul className="nav">
            <li>Каталог</li>
            <li>Контакти</li>
            <li>Кабінет</li>
          </ul>
          <div class="block-btn">
            <div>
              <span>{this.props.countFav}</span>
              <FaRegHeart
                className={`header-fav-btn ${this.state.favOpen && "active"}`}
                onClick={this.favOpenClick}
              />
              {this.state.favOpen && (
                <ul className="shop-cart">
                  {this.props.favorites.map((favorite) => (
                    <Favorite key={favorite.article} card={favorite} />
                  ))}
                </ul>
              )}
            </div>
            <div>
              <span>{this.props.countCart}</span>
              <FaShoppingCart
                className={`shop-cart-btn ${this.state.cartOpen && "active"}`}
                onClick={this.cartOpenClick}
              />
              {this.state.cartOpen && (
                <ul className="shop-cart">
                  {this.props.orders.map((order) => (
                    <Order key={order.article} card={order} />
                  ))}
                </ul>
              )}
            </div>
          </div>
        </div>
      </header>
    );
  }
}
