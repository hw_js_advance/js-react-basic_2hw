import React from "react";
import { RiDeleteBin6Line } from "react-icons/ri";

export class Order extends React.Component {
  render() {
    return (
      <li className="order">
        <img src={"./img/" + this.props.card.img} alt="" />
        <p>{this.props.card.name}</p>
        <b>{this.props.card.price} грн.</b>
        <span>
          <RiDeleteBin6Line />
        </span>
      </li>
    );
  }
}
